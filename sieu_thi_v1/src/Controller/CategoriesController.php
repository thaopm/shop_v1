<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Utility\Text;
use Cake\Utility\Inflector;

use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use Cake\Auth\DigestAuthenticate;


class CategoriesController extends AppController
{   
     
    public function index()
    {
        $categories = TableRegistry::get('Categories');
        
        $category = $categories->find('all');
        $this->set('category',$category);
        
    }

   
    public function view($id = null)
    {   
        $categories = TableRegistry::get('Categories');
        $category = $categories->get($id);

        $category->slug = strtolower(Text::slug($category->name,'-'));
        // 
        $products = TableRegistry::get('Products');
        $product = $products->find('all',[
            'contain'=>['Categories']
        ])->where(['category_id'=>$id]);
        $this->set('products',$product);
        
        $this->set('category', $category);
        $this->set('_serialize', ['category']);
    }

    public function add()
    {
        $categories = TableRegistry::get('Categories');
        $category = $categories->newEntity();
        if ($this->request->is('post')) {
            // 
            $category = $categories->patchEntity($category, $this->request->getData()); 
            $category->slug = strtolower(Text::slug($category->name,'-'));
            if ($categories->save($category)) {
                $this->Flash->success(__('The category has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $this->set(compact('category'));
        $this->set('_serialize', ['category']);
    }

    public function edit($id = null)
    {
        $categories = TableRegistry::get('Categories');
        $category = $categories->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
         
            $category = $categories->patchEntity($category, $this->request->getData());
            $category->slug = strtolower(Text::slug($category->name,'-'));  
            if ($categories->save($category)) {
                $this->Flash->success(__('save success.'));

               return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('save fail.'));
        }
        $this->set(compact('category'));
        $this->set('_serialize', ['category']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $category = $categories->get($id);
        if ($categories->delete($category)) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
