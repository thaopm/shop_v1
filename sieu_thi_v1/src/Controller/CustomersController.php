<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\Authenticate;
use Cake\Event\Event;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;

class CustomersController extends AppController
{
    // public $components = array('Paginator');
    public $paginate = [
        'limit' => 2,
        'order' => [
            'Customers.id' => 'asc'
        ]
    ];

	public function index(){
        $customers = TableRegistry::get('Customers');
        
        $customer = $customers->find('all');
        $this->set('customers',$customer);
    }

	public function add(){
		$customers = TableRegistry::get('Customers');
        $customer = $customers->newEntity();
        if ($this->request->is('post')) {
            $customer = $customers->patchEntity($customer, $this->request->getData());
            if ($customers->save($customer)) {
                $this->Flash->success(__('The customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customer could not be saved. Please, try again.'));
        }

        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
	}

	public function view($id = null){
		$customers = TableRegistry::get('Customers');
        $customer = $customers->get($id,[
                'contain' => [ 'Orders']
            ]
        );

        $orders = TableRegistry::get('Orders');
        $order = $orders->find('all')->where(['customer_id'=>$id]);
        $this->set('orders',$order);

        $this->set('customer', $customer);
        $this->set('_serialize', ['customer']);
	}

	public function edit($id = null){
		$customers = TableRegistry::get('Customers');
		$customer = $customers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customer = $customers->patchEntity($customer, $this->request->getData());
            if ($customers->save($customer)) {
                $this->Flash->success(__('The customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customer could not be saved. Please, try again.'));
        }
        $this->set(compact('customer'));
        $this->set('_serialize', ['customer']);
	}

	public function delete($id = null){
		$customers = TableRegistry::get('Customers');

		$this->request->allowMethod(['post', 'delete']);
        $customer = $customers->get($id);
        if ($customers->delete($customer)) {
            $this->Flash->success(__('The customer has been deleted.'));
        } else {
            $this->Flash->error(__('The customer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
	}


}


?>