<?php
namespace App\Controller\Home;

use App\Controller\AppController;
class PagesController extends AppController
{
    public function initialize(){
        $this->viewBuilder()->setLayout('client');
    }
    /**
     * Displays a view
     *
     * @param string ...$path Path segments.
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function home(){

    }
}
?>