<?php 
namespace App\Controller\Home;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

class ProductsController extends AppController{
	public $paginate =['limit' => 20];
	public function initialize(){
		$this->viewBuilder()->setLayout('client');
		$session = $this->request->session();
	}
	public function admin_index(){
		
	}
	public function index($slug=null){
		if($slug){
			$data = TableRegistry::get('Products');
			$option = ['conditions' => ['Categories.slug'=>$slug]];
			$query = $data->find('all',$option)->contain('Categories');
			$this->paginate($query);
			$this->set('query',$query);
			// categorys
			$categorys = TableRegistry::get('Categories');
			$query2 = $categorys->find('all',[
				'fields'=>['id','name','slug']
				]);
			$this->set('query2',$query2);
		}
		else{
			$categorys = TableRegistry::get('Categories');
			$query2 = $categorys->find('all',[
				'fields'=>['id','name','slug']
				]);
			$this->set('query2',$query2);

			$products = TableRegistry::get('Products');
			$query = $products->find('all')->contain('Categories');
			$this->paginate($query);
			$this->set('query',$query);
		}

	}
	// indexj
	// ..........................................
	public function single($slug=null,$id=null){
		// view products
		$data = TableRegistry::get('Products');
		$option = ['conditions' => ['Categories.slug'=>$slug,'Products.id'=>$id]];
		$product = $data->find('all',$option)->contain('Categories');
        $this->set('product', $product);
        $this->set('_serialize', ['product']);
		// menu
		$categorys = TableRegistry::get('Categories');
		$query2 = $categorys->find('all');
		$this->set('query2',$query2);
	}
	// single
	// -----------------------------------
	public function addCart($id=null){
		if($this->request->is('post')){
			$data = TableRegistry::get('Products');
			$option = ['conditions' => ['Products.id'=>$id]];
			$product = $data->find('all',$option);
			foreach ($product as $value) {
				$id = $value['id'];
				$title = $value['name'];
				$price = $value['price_out'];
			}
			if($this->request->session()->check('cart.'.$id)){
				$cart = $this->request->session()->read('cart.'.$id);
				$cart['quantity']+=1;
				$this->request->session()->write('cart.'.$id,$cart);
				return $this->redirect($this->referer());
			}else{
				$item_cart = ['id'=>$id,'title'=>$title,'price'=>$price,'quantity' =>1];
				$this->request->session()->write('cart.'.$id,$item_cart);
				return $this->redirect($this->referer());
			}
		}
	}
}

?>