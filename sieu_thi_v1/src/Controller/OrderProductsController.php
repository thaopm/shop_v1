<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

class OrderProductsController extends AppController
{
    public function index(){
        $orderProducts = TableRegistry::get('OrderProducts');
        $orderProduct = $orderProducts->find('all');

        $this->set('orderProducts',$orderProduct);
        $this->set('_serialize', ['orderProducts']);
    }

    public function view($id = null){
        $orderProducts = TableRegistry::get('OrderProducts');
        $orderProduct = $orderProducts->get($id,[
                // 'contain' => ['OrderProducts']
            ]
        );

        $this->set('orderProduct', $orderProduct);
        $this->set('_serialize', ['orderProduct']);
    }

    public function add(){
        $orderProducts = TableRegistry::get('OrderProducts');
        $orderProduct = $orderProducts->newEntity();
        if ($this->request->is('post')) {
            $orderProduct = $orderProducts->patchEntity($orderProduct, $this->request->getData());

            if ($orderProducts->save($orderProduct)) {
                $this->Flash->success(__('The orderProducts has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The orderProducts could not be saved. Please, try again.'));
        }
        $products = TableRegistry::get('Products')->find('list');
        $orders = TableRegistry::get('Orders')->find('list');

        $this->set('products',$products);
        $this->set('orders',$orders);  
        
        $this->set('orderProduct',$orderProduct);
        $this->set('_serialize', ['orderProduct']);
    }

    public function edit($id = null){
        $orderProducts = TableRegistry::get('OrderProducts');
        $orderProduct = $orderProducts->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $orderProduct = $orderProducts->patchEntity($orderProduct, $this->request->getData());

            if ($orderProducts->save($orderProduct)) {
                $this->Flash->success(__('The orderProducts Meta has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The orderProducts could not be saved. Please, try again.'));
        }
        
        $products = TableRegistry::get('Products')->find('list');
        $orders = TableRegistry::get('Orders')->find('list');

        $this->set('products',$products);
        $this->set('orders',$orders);  

        $this->set('orderProduct',$orderProduct); 
        $this->set('_serialize', ['orderProduct']);
    }

    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $orderProducts = TableRegistry::get('OrderProducts');
        $orderProduct = $orderProducts->get($id);
        if ($orderProducts->delete($orderProduct)) {
            $this->Flash->success(__('The orderProduct has been deleted.'));
        } else {
            $this->Flash->error(__('The orderProduct could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']); 
    }










}
?>