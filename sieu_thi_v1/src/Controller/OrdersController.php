<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

class OrdersController extends AppController
{
    public function index(){
        $orders = TableRegistry::get('Orders');
        $order = $orders->find('all');

        $this->set('orders',$order);
        $this->set('_serialize', ['orders']);
    }

    public function view($id = null){
        $orders = TableRegistry::get('Orders');
        $order = $orders->get($id,[
                // 'contain' => ['OrderProducts']
            ]
        );

        $this->set('orders', $order);
        $this->set('_serialize', ['orders']);
    }

    public function add(){
        $orders = TableRegistry::get('Orders');
        $order = $orders->newEntity();
        if ($this->request->is('post')) {
            $order = $orders->patchEntity($order, $this->request->getData());
            if ($orders->save($order)) {
                $this->Flash->success(__('The orders has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }

        $customers = TableRegistry::get('Customers')->find('list');
        $this->set('customers',$customers);

        $this->set(compact('order'));
        $this->set('_serialize', ['order']);
    }

    public function edit($id = null){
        $orders = TableRegistry::get('Orders');
        $order = $orders->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $order = $orders->patchEntity($order, $this->request->getData());

            if ($orders->save($order)) {
                $this->Flash->success(__('The order Meta has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        
        $customers = TableRegistry::get('Customers')->find('list');
        $this->set('customers',$customers); 
        $this->set('orders',$order); 
        $this->set('_serialize', ['order']);
    }

    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $orders = TableRegistry::get('Orders');
        $order = $orders->get($id);
        if ($orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']); 
    }










}
?>