<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;

use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

class ProductMetasController extends AppController
{
    public function index(){
        $product_metas = TableRegistry::get('ProductMetas');
        $product_meta = $product_metas->find('all',[
            // 'contain'=>'Products'
        ]);
        $this->set('product_metas',$product_meta);                               
    }

    public function add(){
        $product_metas = TableRegistry::get('ProductMetas');
        $product_meta = $product_metas->newEntity();

        if ($this->request->is('post')) {
            $product_meta = $product_metas->patchEntity($product_meta, $this->request->getData());
            $product_meta->slug = strtolower(Text::slug($product_meta->name,'-'));

            if ($product_metas->save($product_meta)) {
                $this->Flash->success(__('The product Metas has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product metas could not be saved. Please, try again.'));
        }
        $products = TableRegistry::get('Products')->find('list');
        // $this->set(compact('product_meta','$products'));
        $this->set('product_metas',$product_meta); 
        $this->set('products',$products); 
        $this->set('_serialize', ['product_meta']);
    }
    public function edit($id = null){
        $product_metas = TableRegistry::get('ProductMetas');
        $product_meta = $product_metas->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $product_meta = $product_metas->patchEntity($product_meta, $this->request->getData());

            if ($product_metas->save($product_meta)) {
                $this->Flash->success(__('The product Meta has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product meta could not be saved. Please, try again.'));
        }
        
        $products = TableRegistry::get('Products')->find('list');
        $this->set('product_metas',$product_meta); 
        $this->set('products',$products); 
        $this->set('_serialize', ['product_meta']);

    }

    public function view($id = null){
        $product_metas = TableRegistry::get('ProductMetas');
        $product_meta = $product_metas->get($id,[
                // 'contain' => ['Products']
            ]
        );

        $this->set('product_meta', $product_meta);
        $this->set('_serialize', ['product_meta']);
    }
    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $product_metas = TableRegistry::get('ProductMetas');
        $product_meta = $product_metas->get($id);
        if ($product_metas->delete($product_meta)) {
            $this->Flash->success(__('The productMeta has been deleted.'));
        } else {
            $this->Flash->error(__('The productMeta could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


}
?>