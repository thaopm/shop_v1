<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

class ProductsController extends AppController
{
    public $paginate = [
        'limit' => 2,
        'order' => [
            'Products.id' => 'asc'
        ]
    ];
 
    public function adminIndex()
    {
        $products = TableRegistry::get('Products');
        $product = $products->find('all',[
            'contain'=>['Categories']
        ]);
        $this->set('products',$product);
    }

  
    public function adminView($id = null)
    {
        $products = TableRegistry::get('Products');
        $product = $products->get($id,[
                'contain' => ['Categories', 'OrderProducts', 'ProductMetas']
            ]
        );

        $orderProducts = TableRegistry::get('OrderProducts');
        $orderProduct = $orderProducts->find('all')->where(['product_id'=>$id]);
        $this->set('order_products',$orderProduct);

        $product_metas = TableRegistry::get('ProductMetas');
        $product_meta = $product_metas->find('all')->where(['product_id'=>$id]);
        $this->set('product_metas',$product_meta);

        $this->set('product', $product);
        $this->set('_serialize', ['product']);
    }

  
    public function adminAdd()
    {
        $products = TableRegistry::get('Products');
        $product = $products->newEntity();
        if ($this->request->is('post')) {
            $product = $products->patchEntity($product, $this->request->getData());
            if ($products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $categories = TableRegistry::get('Categories')->find('list');
        $this->set(compact('product', 'categories'));
        $this->set('_serialize', ['product']);
    }

    public function adminEdit($id = null)
    {
        $products = TableRegistry::get('Products');
        $product = $products->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $products->patchEntity($product, $this->request->getData());
            if ($products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $categories = $products->Categories->find('list', ['limit' => 200]);
        $this->set(compact('product', 'categories'));
        $this->set('_serialize', ['product']);
    }

    public function adminDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $products->get($id);
        if ($products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
