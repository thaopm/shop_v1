<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Product_meta extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
