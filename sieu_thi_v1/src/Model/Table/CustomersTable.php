<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


class CustomersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Orders', [
            'foreignKey' => 'customer_id'
        ]);
        
    }

   
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('username', 'create')
            ->notEmpty('username');
        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');
        $validator
            ->requirePresence('fullname', 'create')
            ->notEmpty('fullname');
        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email');
        $validator
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');
        $validator
            ->requirePresence('address', 'create')
            ->notEmpty('address');        

        $validator
            ->allowEmpty('avatar');

       
        return $validator;
    }


    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
    
}
