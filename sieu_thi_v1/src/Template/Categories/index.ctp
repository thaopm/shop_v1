

<div class="categories index large-12 medium-12 columns content">
    <h3><?= __('Categories') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($category as $cate): ?>
            <tr>
                <td><?= $this->Number->format($cate->id) ?></td>
                <td><?= h($cate->name) ?></td>
                <td><?= h($cate->slug) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $cate->id]) ?> |
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cate->id]) ?> |
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $cate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cate->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
      
    </div>
   
</div>
