<?php $cart_products = $this->request->session()->read('cart'); ?>
<!-- container -->
<div class="container">
<div class="container text-center">
			<div class="col-md-5 col-sm-12">
				<div class="bigcart"></div>
				<h1>Your shopping cart</h1>
			</div>
			
			<div class="col-md-7 col-sm-12 text-left">
				<ul>
					<li class="row list-inline columnCaptions">
						<span class="stt">STT</span>
						<span class="itemName">TÊN SẢN PHẨM</span>
						<span class="quantity">SL</span>
						<span class="price_cart">GIÁ SẢN PHẨM</span>
					</li>
					<?php foreach($cart_products as $products): ?>
					<li class="row">
						<span class="stt">1</span>
						<span class="itemName"><?= $products['title'];?></span>
						<span class="quantity"><input type="text" name="quantity" value="<?= $products['quantity'];?>"></span>
						<span class="delete"><a href="<?= $products['id'];?>">XÓA</a></span>
						<span class="price_cart"><?= $products['price'];?></span>
					</li>
					<?php endforeach ; ?>
					<li class="row totals">
						<span class="itemName">Total:</span>
						<span class="price_total">$1694.43</span>
						<span class="order"> <a class="text-center">ORDER</a></span>
					</li>
				</ul>
			</div>

		</div>

		<!-- The popover content -->
		<!-- JavaScript includes -->

		<!-- <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> -->
		<?php echo $this->Html->script('home/jquery-1.11.0.min'); ?> 
		<?php echo $this->Html->script('home/customjs'); ?>
		<!-- <script src="assets/js/bootstrap.min.js"></script> -->
		<?php echo $this->Html->script('home/bootstrap.min'); ?>
		<!-- <script src="assets/js/customjs.js"></script> -->
</div>
<!-- container -->