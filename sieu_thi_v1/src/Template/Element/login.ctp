<!-- pop-up-box -->
<!-- <script type="text/javascript" src="js/modernizr.custom.min.js"></script> -->
<?php echo $this->Html->script('home/modernizr.custom.min'); ?>    
<!-- <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" /> -->
<?php echo $this->Html->css('home/popuo-box'); ?>
<!-- <script src="js/jquery.magnific-popup.js" type="text/javascript"></script> -->
<?php echo $this->Html->script('home/jquery.magnific-popup'); ?>
<!--//pop-up-box -->
<div id="small-dialog2" class="mfp-hide">
	<h3>Create Account</h3> 
	<div class="social-sits">
		<ul>
			<li><a class="fb" href="#">Connect with Facebook</a></li>
			<li><a class="fb google" href="#">Connect with Google</a></li>
		</ul>
	</div>
	<div class="signup">
		<form>
			<input type="text" class="email" placeholder="Email" required="required" pattern="([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?" />
			<input type="password" placeholder="Password" required="required" pattern=".{6,}" title="Minimum 6 characters required" autocomplete="off" />
			<input type="text" class="email" placeholder="Mobile Number" maxlength="10" pattern="[1-9]{1}\d{9}" title="Enter a valid mobile number" />
			<input type="submit"  value="Sign In"/>
		</form>
	</div>
	<div class="clearfix"> </div>
</div>	
<script>
	$(document).ready(function() {
		$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});
		
	});
</script>	