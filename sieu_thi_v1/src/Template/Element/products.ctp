<?php foreach ($query as $value):?>
<div class="col-md-3 fashion-grid">
	<a href="#"><?php echo $this->Html->image($value['avatar'],['width'=>'276','height'=>'276']); ?>
		<div class="product">
			<h3><?php echo $value['name'] ?></h3>
			<p><span></span> <?php echo $this->Number->format($value['price_out'],['places' => 2,'after'=> ' VNĐ']); ?></p>	 
		</div>							 
	</a>
	<div class="fashion-view"><span></span>
		<div class="clearfix"></div>
		<h4>
			<?= $value->has('category') ? $this->Html->link('Chi Tiết','/home/'.$value->category->slug.'/'.$value['id']) : '' ?>
		</h4>
	</div>
</div>
<?php endforeach ; ?>