<!-- men -->
<div class="men">
	<!-- container -->
	<div class="container">
		<div class="col-md-9 fashions">
			<div class="title">
				<h3>SẢN PHẨM NỔI BẬT</h3>
			</div>
			<div class="fashion-section">
				<div class="fashion-grid1">
					 <?php echo $this->element('products'); ?>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="paginator">
		        <ul class="pagination">
		            <?= $this->Paginator->first('<< ' . __('first')) ?>
		            <?= $this->Paginator->prev('< ' . __('previous')) ?>
		            <?= $this->Paginator->numbers() ?>
		            <?= $this->Paginator->next(__('next') . ' >') ?>
		            <?= $this->Paginator->last(__('last') . ' >>') ?>
		        </ul>
			</div>
		</div>
		<div class="col-md-3 side-bar">
			<?php echo $this->element('categories'); ?>
		<div class="clearfix"> </div>
	</div>
	<!-- //container -->
</div>
<!-- //men -->
</body>
</html>