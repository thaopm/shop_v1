<?php $cart = $this->request->session()->read('cart');?>
<!-- single -->
<div class="single">
	<!-- container -->
	<div class="container">
		<div class="single-grids">
			<div class="col-md-9">
				<div class="single-left-left">
				<?php foreach ($product as $value) : ?>
					<ul id="etalage" class="etalage" style="display: block; width: 300px; height: 533px;">
						<li class="etalage_thumb thumb_1" style="display: none; opacity: 0; background-image: none;">
							<a href="optionallink.html">
								<?php echo $this->Html->image($value['avatar'],['class' => 'etalage_thumb_image','style' =>['display: inline; width: 300px; height: 400px; opacity: 1','escape'=>false]]); ?>
								<?php echo $this->Html->image($value['avatar'],['class' => 'etalage_source_image']); ?>
							</a>
							<!-- ahihi -->
						</li>
						<li class="etalage_thumb thumb_2" style="display: none; opacity: 0; background-image: none;">
							<?php echo $this->Html->image($value['avatar'],['class' => 'etalage_thumb_image','style' =>['display: inline; width: 300px; height: 400px; opacity: 1','escape'=>false]]); ?>
							<?php echo $this->Html->image($value['avatar'],['class' => 'etalage_source_image']); ?>
						</li>
						<li class="etalage_thumb thumb_3" style="display: none; opacity: 0; background-image: none;">
							<?php echo $this->Html->image($value['avatar'],['class' => 'etalage_thumb_image','style' =>['display: inline; width: 300px; height: 400px; opacity: 1','escape'=>false]]); ?>
							<?php echo $this->Html->image($value['avatar'],['class' => 'etalage_source_image']); ?>
						</li>
					    <li class="etalage_thumb thumb_4 etalage_thumb_active" style="display: list-item; opacity: 1; background-image: none;">
							<?php echo $this->Html->image($value['avatar'],['class' => 'etalage_thumb_image','style' =>['display: inline; width: 300px; height: 400px; opacity: 1','escape'=>false]]); ?>
							<?php echo $this->Html->image($value['avatar'],['class' => 'etalage_source_image']); ?>
						</li>
					</ul>
				<?php endforeach ; ?>

					 <div class="clearfix"></div>		
				</div>
				<div class="single-left-right">
					<div class="single-left-info">
					<?php foreach ($product as $value): ?>
						<h3><?php echo $value['name'] ?></h3>
						<p><?php echo $this->Number->format($value['price_out'],['places' => 2,'after'=> ' VNĐ']); ?></p>
					</div>
					<div class="select-size">
						<p>Select a size</p>
							<ul>
								 <li><a href="#">S</a></li>
								 <li><a href="#">M</a></li>
								 <li><a href="#">L</a></li>
								 <li><a href="#">XL</a></li>
							</ul>
						<div class="buy-now">
							<?= $this->Form->postLink('ADD TO CART','/home/products/add-cart/'.$value['id']); ?>
						</div>
						<?php endforeach; ?>
						<div class="wishlist">
							<a class="play-icon popup-with-zoom-anim" href="#small-dialog2"></a>
							<?php echo $this->element('login'); ?>
						</div>
						<div class="clearfix"> </div>
						<div class="free">
							<p>20 day returns Free Delivery *</p>
						</div>
						<div class="delivery">
							<a class="play-icon popup-with-zoom-anim" href="#small-dialog">Check delivery options</a>
							<div id="small-dialog" class="mfp-hide">
								<h3>DELIVERY TIME & COD AVAILABILITY</h3>
								<div class="social-sits">
									<p>Please enter your PIN Code to check delivery time & Cash On Delivery availability</p>
								</div>
								<div class="signup">
										<form>
											<input type="text" class="email" placeholder="Pin" maxlength="6" required="required" pattern="[1-9]{1}\d{5}"/>
											<input type="submit"  value="Submit"/>
										</form>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				<div class="product-details">
					<h3>PRODUCT DETAILS</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a est at leo dictum 
						pharetra vel sit amet tellus. Vivamus vitae enim sit amet orci euismod consequat. 
						Nulla porttitor vitae arcu ut porttitor. Pellentesque auctor enim eros, et malesuada 
						ipsum suscipit quis. Aliquam sed ex risus. Etiam hendrerit velit luctus facilisis 
						mollis. Integer auctor consequat est. Integer viverra fringilla finibus. Nunc id 
						dignissim enim. 
					</p>
				</div>
				<div class="related">
					<h3>RELATED PRODUCTS</h3>
					<div class="related-grids">
						<div class="related-grid">
							<div class="col-md-9 related-left">
								<div class="col-md-3 related-left-left">
									<?php echo $this->Html->image('c1.jpg'); ?>
								</div>
								<div class="col-md-9 related-left-right">
									<h5>Vestibulum</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a est at leo dictum 
										pharetra vel sit amet tellus.
									</p>
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="col-md-3 related-right">
								<p>$ 19</p>
								<a href="#">Add to cart</a>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="related-grid">
							<div class="col-md-9 related-left">
								<div class="col-md-3 related-left-left">
									<?php echo $this->Html->image('c2.jpg'); ?>
								</div>
								<div class="col-md-9 related-left-right">
									<h5>Vestibulum</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a est at leo dictum 
										pharetra vel sit amet tellus.
									</p>
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="col-md-3 related-right">
								<p>$ 19</p>
								<a href="#">Add to cart</a>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="related-grid">
							<div class="col-md-9 related-left">
								<div class="col-md-3 related-left-left">
									<?php echo $this->Html->image('c3.jpg'); ?>
								</div>
								<div class="col-md-9 related-left-right">
									<h5>Vestibulum</h5>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a est at leo dictum 
										pharetra vel sit amet tellus.
									</p>
								</div>
								<div class="clearfix"> </div>
							</div>
							<div class="col-md-3 related-right">
								<p>$ 19</p>
								<a href="#">Add to cart</a>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 side-bar">
			<?php echo $this->element('categories'); ?>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //container -->
</div>
<!-- //single -->
</body>
</html>