<title>Shop | Nhóm Thực Tập 1</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<?php echo $this->Html->css('home/bootstrap.min'); ?>
<?php echo $this->Html->css('home/custom'); ?> 
<?php echo $this->Html->charset(); ?>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstarp-css -->
<?php echo $this->Html->css('home/bootstrap'); ?>
<!--// bootstarp-css -->
<!-- css -->
<?php echo $this->Html->css('home/style'); ?>
<!--// css -->
<?php echo $this->Html->script('home/jquery.min'); ?>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
<!--/fonts-->
<!-- dropdown -->
<?php echo $this->Html->script('home/jquery.easydropdown'); ?>
<?php echo $this->Html->css('home/nav'); ?>
<?php echo $this->Html->script('home/scripts'); ?>
<!--js-->
<?php echo $this->Html->css('home/etalage'); ?>
<?php echo $this->Html->script('home/jquery.etalage.min'); ?>
<script>
        jQuery(document).ready(function($){

            $('#etalage').etalage({
                thumb_image_width: 300,
                thumb_image_height: 400,
                source_image_width: 800,
                source_image_height: 1000,
                show_hint: true,
                click_callback: function(image_anchor, instance_id){
                    alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
                }
            });

        });
    </script>
<!--/js-->
<?php echo $this->Html->script('home/easyResponsiveTabs'); ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#horizontalTab').easyResponsiveTabs({
                    type: 'default', //Types: default, vertical, accordion           
                    width: 'auto', //auto or any width like 600px
                    fit: true   // 100% fit in a container
                });
            });
           </script>    
<!-- start-smoth-scrolling -->
<?php echo $this->Html->script('home/move-top'); ?>
<?php echo $this->Html->script('home/easing'); ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){     
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
</head>
<body>
<!-- header -->
<div class="header">
    <!-- container -->
    <div class="container">
        <!-- header-top -->
        <div class="header-top">
            <div class="header-logo">
                <?php echo $this->Html->link($this->Html->image("logo.png", ["alt" => "logo"]),"/home",['escape' => false]); ?>
            </div>
            <div class="header-right">
                <ul>
                    <li class="phone">+84948-246-254</li>
                    <li class="mail"><a href="mailto:example@mail.com">loilv.295@gmail.com</a></li>
                    <li class="checkout">
                        <?php echo $this->element('cart'); ?>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <!-- //header-top -->
        <div class="top-nav">
            <span class="menu"><img src="img/menu.png" alt=""></span>
            <ul class="nav">
                <li class="dropdown1"><?= $this->Html->link('TRANG CHỦ','/home/'); ?>
                    <!-- <ul class="dropdown2">
                        <li><a href="single.html">lorem</a></li>
                        <li><a href="single.html">dorem sia</a></li>
                        <li><a href="single.html">erik</a></li>
                        <li><a href="single.html">ipsum padamans</a></li>
                        <li><a href="single.html">behance</a></li>
                    </ul> -->
                </li>
                <li class="dropdown1"><?= $this->Html->link('SẢN PHẨM','/home/san-pham/'); ?>
                   <!--  <ul class="dropdown2">
                        <li><a href="men.html">Clothing</a></li>
                        <li><a href="men.html">Footwear</a></li>
                        <li><a href="men.html">Watches</a></li>
                        <li><a href="men.html">Accessories</a></li>
                    </ul> -->
                </li>               
                <li><a href="about.html">ABOUT US</a></li>            
                <li><a href="404.html">SUPPORT</a></li>
            </ul>
        </div>
        <div class="search">
            <?php  echo $this->Form->create('products', $options = array('type'=>'GET')); ?>
            <?php echo $this->Form->input('', $options = array('Placeholder'=>'Search...')); ?>
            <?php  echo $this->Form->end(); ?>
        </div>
        <div class="clearfix"> </div>
        <!-- script-for-menu -->
     <script>
            $("span.menu").click(function(){
                $(" ul.nav").slideToggle("slow" , function(){
                });
            });
     </script>
    </div>
    <!-- //container -->
</div>
<!-- //header -->
<?= $this->Flash->render() ?>
<?php echo $this->fetch('content'); ?>
<?php echo $this->element('footer'); ?>