<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <title>Siêu Thị Mini</title>

    <!-- Bootstrap -->
    <?php echo $this->Html->css('template/bootstrap/dist/css/bootstrap.min.css');?>

    <!-- Font Awesome -->
    <?php echo $this->Html->css('template/font-awesome/css/font-awesome.min.css');?> 
    
    <!-- bootstrap-progressbar -->
     <?php echo $this->Html->css('template/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css');?>
    
    <!-- Custom Theme Style -->
     <?php echo $this->Html->css('custom.min.css');?>
     <?php echo $this->Html->css('style.css');?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Admin !</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="img/111.jpg" alt=" " class="img-circle profile_img">
                <!--<?php echo $this->Html->image('111.img', ['fullBase' => false]);?>-->
              </div>
              <div class="profile_info">
                <span>Welcome</span>
                <h2>Thao pm</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="index.html">Dashboard</a></li>
                      <li><a href="index2.html">Dashboard2</a></li>
                      <li><a href="index3.html">Dashboard3</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Users <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><?= $this->Html->link(__('New User'), ['controller' => 'Users','action' => 'add']) ?></li>
                      <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users','action' => 'index']) ?></a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Customers <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers','action' => 'add']) ?></li>
                      <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers','action' => 'index']) ?></a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Categories <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><?= $this->Html->link(__('New Category'), ['controller' => 'Categories','action' => 'add']) ?></li>
                      <li><?= $this->Html->link(__('List Categories'), ['controller' => 'Categories', 'action' => 'index']) ?></li>
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> Products <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products','action' => 'adminAdd']) ?></li>
                      <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'adminIndex']) ?></li>
                    </ul>
                  </li>
                   <li><a><i class="fa fa-table"></i> Products Metas  <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><?= $this->Html->link(__('New Product Meta'), ['controller' => 'ProductMetas','action' => 'add']) ?></li>
                      <li><?= $this->Html->link(__('List Products Meta'), ['controller' => 'ProductMetas', 'action' => 'index']) ?></li>
                      
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Orders <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders','action' => 'add']) ?></li>
                       <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?></li>
                      
                    </ul>
                  </li>
                   <li><a><i class="fa fa-clone"></i>Product orders <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><?= $this->Html->link(__('New Product Order'), ['controller' => 'OrderProducts','action' => 'add']) ?></li>
                       <li><?= $this->Html->link(__('List Products Orders'), ['controller' => 'OrderProducts', 'action' => 'index']) ?></li>
                    </ul>
                  </li>
                  
                </ul>
              </div>
              
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="img/111.jpg" alt="">tpm
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> <?= $this->Html->link(__('Logout'), ['action' => 'logout']) ?></a></li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green">6</span>
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
                        <span>
                          <span>John Smith</span>
                          <span class="time">3 mins ago</span>
                        </span>
                        <span class="message">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="text-center">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                   <?= $this->fetch('content') ?>
                  
                </div>
                <div class="clearfix"></div>
              </div>
            </div>

          </div>
          <br />
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            TMP - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <?php echo $this->Html->script('template/jquery/dist/jquery.min.js'); ?>
   
    <!-- Bootstrap -->
    <?php echo $this->Html->script('template/bootstrap/dist/js/bootstrap.min.js'); ?>
    
    <!-- bootstrap-progressbar -->
    <?php echo $this->Html->script('template/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>
    
    <!-- Skycons -->
    <?php echo $this->Html->script('template/skycons/skycons.js'); ?>

    <!-- Custom Theme Scripts -->
    <?php echo $this->Html->script('custom.min.js'); ?>
    
  </body>
</html>
