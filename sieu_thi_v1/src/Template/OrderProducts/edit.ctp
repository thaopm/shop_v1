<?php
// pr($orders);die;
?>

<div class="products form large-12 medium-12 columns content">
    <?= $this->Form->create($orderProduct) ?>
    <fieldset>
        <legend><?= __('Edit Order Product') ?></legend>
        <?php
            echo $this->Form->control('product_id', ['options' => $products, 'empty' => false]);
            echo $this->Form->control('order_id', ['options' => $orders, 'empty' => false]);
            echo $this->Form->control('quantity');
            echo $this->Form->control('type');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
