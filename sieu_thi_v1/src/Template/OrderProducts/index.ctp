
<div class="categories index large-12 medium-12 columns content">
    <h3><?= __('Order Product') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('quanlity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orderProducts as $orderProduct): ?>
            <tr>
                <td><?= $this->Number->format($orderProduct->id) ?></td>
                <td><?= h($orderProduct->product_id) ?></td>
                <td><?= h($orderProduct->order_id) ?></td>
                <td><?= h($orderProduct->quantity) ?></td>
                <td><?= h($orderProduct->type) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $orderProduct->id]) ?> |
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $orderProduct->id]) ?> |
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $orderProduct->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderProduct->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
   
</div>


