

<div class="products view large-12 medium-12 columns content">
    <h3><?= h($orderProduct->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('id') ?></th>
            <td><?= h($orderProduct->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('product_id') ?></th>
            <td><?= h($orderProduct->product_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('order_id') ?></th>
            <td><?= h($orderProduct->order_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('quantity') ?></th>
            <td><?= h($orderProduct->quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('type') ?></th>
            <td><?= h($orderProduct->type) ?></td>
        </tr>
        
    </table>
   
</div>
