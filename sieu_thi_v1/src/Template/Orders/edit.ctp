<?php
// pr($customers);die;
?>

<div class="products form large-12 medium-12 columns content">
    <?= $this->Form->create($orders) ?>
    <fieldset>
        <legend><?= __('Edit Order') ?></legend>
        <?php
            echo $this->Form->control('customer_id', ['options' => $customers, 'empty' => true]);
            echo $this->Form->control('sum_price');
            echo $this->Form->control('status');
            echo $this->Form->control('end_date');
            echo $this->Form->control('note');
            echo $this->Form->control('modified');
            echo $this->Form->control('created');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
