

<!-- <div class="products view large-12 medium-12 columns content">
    <h3><?= h($orders->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('id') ?></th>
            <td><?= h($orders->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('customer') ?></th>
            <td><?= h($orders->customer_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('sum price') ?></th>
            <td><?= h($orders->sum_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('status') ?></th>
            <td><?= h($orders->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('end date') ?></th>
            <td><?= h($orders->end_date) ?></td>
        </tr>
         <tr>
            <th scope="row"><?= __('note') ?></th>
            <td><?= h($orders->note) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('modified') ?></th>
            <td><?= h($orders->modified) ?></td>
        </tr>
         <tr>
            <th scope="row"><?= __('created') ?></th>
            <td><?= h($orders->created) ?></td>
        </tr>
    </table>
   
</div>
 -->
<?php
    // pr($orders);die;
?>
<div class="" role="main">
  <div class="">
  
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Order Info </h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            <form class="form-horizontal form-label-left" novalidate>

              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">customer_id <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" placeholder="both name(s) e.g Jon Doe" required="required" type="text" value="<?php echo $orders['customer_id'];?>">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sum_price">sum_price <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="sum_price" name="sum_price" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $orders['sum_price'];?>">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">status <span class="required" >*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="number" id="status" name="status" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" value ="<?php echo $orders['status'];?>">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="end_date">end_date <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="end_date" name="end_date" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" value="<?php echo $orders['end_date'];?>">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="note">note <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <textarea id="note" required="required" name="note" class="form-control col-md-7 col-xs-12" ><?php echo $orders['note'];?></textarea>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="created">created <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" id="created" name="created" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" value="<?php echo $orders['created'];?>">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="modified">modified <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="select" id="modified" name="modified" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" value="<?php echo $orders['modified'];?>">
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <button type="submit" class="btn btn-primary" >Cancel</button>
                  <button id="send" type="submit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>





