

<div class="products form large-12 medium-12 columns content">
    <?= $this->Form->create($product_metas) ?>
    <fieldset>
        <legend><?= __('Edit ProductMeta') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('value');
            echo $this->Form->control('status');
            echo $this->Form->control('product_id', ['options' => $products, 'empty' => false]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
