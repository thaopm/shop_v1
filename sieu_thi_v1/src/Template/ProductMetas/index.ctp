
<div class="categories index large-12 medium-12 columns content">
    <h3><?= __('Product Metas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($product_metas as $product_meta): ?>
            <tr>
                <td><?= $this->Number->format($product_meta->id) ?></td>
                <td><?= h($product_meta->name) ?></td>
                <td><?= h($product_meta->slug) ?></td>
                <td><?= h($product_meta->value) ?></td>
                <td><?= h($product_meta->status) ?></td>
                <td><?= h($product_meta->product_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $product_meta->id]) ?> |
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $product_meta->id]) ?> |
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $product_meta->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product_meta->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
   
</div>


