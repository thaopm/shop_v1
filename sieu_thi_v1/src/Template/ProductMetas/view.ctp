

<div class="products view large-12 medium-12 columns content">
    <h3><?= h($product_meta->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($product_meta->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('value') ?></th>
            <td><?= h($product_meta->value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('status') ?></th>
            <td><?= h($product_meta->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('product_id') ?></th>
            <td><?= h($product_meta->product_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('slug') ?></th>
            <td><?= h($product_meta->slug) ?></td>
        </tr>
        
    </table>
   
</div>
