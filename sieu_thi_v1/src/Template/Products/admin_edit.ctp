

<div class="products form large-12 medium-12 columns content">
    <?= $this->Form->create($product) ?>
    <fieldset>
        <legend><?= __('Edit Product') ?></legend>
        <?php
            echo $this->Form->control('name');
            // echo $this->Form->control('avatar', ['type' => 'file']);
            
            echo $this->Form->control('avatar', ['type' => 'file','url'=> $product->avatar]);
            echo $this->Html->image($product->avatar, ['fullBase' => true]);
            echo $this->Form->control('price_in');
            echo $this->Form->control('price_out');
            echo $this->Form->control('category_id', ['options' => $categories, 'empty' => false]);
            echo $this->Form->control('status');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
