
<div class="products view large-12 medium-12 columns content">
    <h3><?= h($product->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($product->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Avatar') ?></th>
            <td><?php echo $this->Html->image($product->avatar, ['fullBase' => true]);?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price In') ?></th>
            <td><?= h($product->price_in) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price Out') ?></th>
            <td><?= h($product->price_out) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Category') ?></th>
            <td><?= $product->has('category') ? $this->Html->link($product->category->name, ['controller' => 'Categories', 'action' => 'view', $product->category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($product->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($product->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($product->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($product->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Order Products') ?></h4>
        <?php if (!empty($order_products)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Order Id') ?></th>
                <th scope="col"><?= __('Quantity') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($order_products as $orderProducts): ?>
            <tr>
                <td><?= h($orderProducts->id) ?></td>
                <td><?= h($orderProducts->product_id) ?></td>
                <td><?= h($orderProducts->order_id) ?></td>
                <td><?= h($orderProducts->quantity) ?></td>
                <td><?= h($orderProducts->type) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OrderProducts', 'action' => 'view', $orderProducts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OrderProducts', 'action' => 'edit', $orderProducts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OrderProducts', 'action' => 'delete', $orderProducts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderProducts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Product Metas') ?></h4>
        <?php if (!empty($product_metas)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Value') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product_metas as $productMetas): ?>
            <tr>
                <td><?= h($productMetas->id) ?></td>
                <td><?= h($productMetas->name) ?></td>
                <td><?= h($productMetas->value) ?></td>
                <td><?= h($productMetas->status) ?></td>
                <td><?= h($productMetas->product_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ProductMetas', 'action' => 'view', $productMetas->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ProductMetas', 'action' => 'edit', $productMetas->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ProductMetas', 'action' => 'delete', $productMetas->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productMetas->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
