
<?php
echo $this->Html->css('bootstrap');
echo $this->Html->css('style');

?>
<div class="container">

    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" method="post" >
                <fieldset>
                    <h2>Please Sign In</h2>
                    <hr class="colorgraph">
                    <div class="form-group">
                        <!--<input type="text" name="username" id="username" class="form-control input-lg" placeholder="User name">-->
                        <?php echo $this->Form->control('username',['class'=>"form-control input-lg", 'placeholder'=>'Username','label'=>false]);?>
                    </div>
                    <div class="form-group">
                        <!--<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password">-->
                        <?php echo $this->Form->control('password',['class'=>"form-control input-lg", 'placeholder'=>'Password','label'=>false]);?>
                    </div>
                    <span class="button-checkbox">
                        <button type="button" class="btn" data-color="info">Remember Me</button>
                        <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
                        <a href="" class="btn btn-link pull-right">Forgot Password?</a>
                    </span>
                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign In" >
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <a href="" class="btn btn-lg btn-primary btn-block">Register</a>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>

<?php
echo $this->Html->script('scripts');
?>